<?php
/**
 * The main template file
 *
 * @package QuentinBuiteman
 */
?>
<!DOCTYPE html>
<!--#if expr="$HTTP_COOKIE=/fonts\-loaded\=true/" -->
<html <?php language_attributes(); ?> class="fonts-loaded">
<!--#else -->
<html <?php language_attributes(); ?>>
<!--#endif -->

<head><?php wp_head(); ?></head>

<body class='<?php the_field('bg_clr'); ?>'>
    <div id='container'></div>

	<?php wp_footer(); ?>
</body>
</html>
