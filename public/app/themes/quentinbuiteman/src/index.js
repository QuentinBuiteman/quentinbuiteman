// Polyfills
import 'core-js/es6/promise';
import 'core-js/fn/object/assign';

// Libraries
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

// Javascript
import LoadFonts from 'general/LoadFonts';

// Import app content
import App from 'containers/App/App';
import { fetchMenu } from 'containers/App/AppActions';
import { fetchContent } from 'containers/App/components/Routes/RoutesActions';
import { storeHistory, store } from './store';

LoadFonts();

// Render into container
const init = () => {
  // Get container
  const container = document.getElementById('container');

  // Check if the container exists
  if (container === null) { return; }

  // Start rendering
  render(
    <Provider store={store}>
      <ConnectedRouter history={storeHistory}>
        <App />
      </ConnectedRouter>
    </Provider>,
    container,
  );

  // Initialize site menu
  store.dispatch(fetchMenu()).then(
    store.dispatch(fetchContent(storeHistory.location.pathname)),
  );

  // Listen for changes to the current location.
  storeHistory.listen((location) => {
    // Fetch content
    store.dispatch(fetchContent(location.pathname));
  });

  // Set scroll restoration to manual for better navigating experience
  if ('scrollRestoration' in history) {
    history.scrollRestoration = 'manual';
  }
};

// Run init when dom is loaded
window.addEventListener('load', init, false);
