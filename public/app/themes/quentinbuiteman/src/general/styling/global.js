import { injectGlobal } from 'styled-components';
import { colors } from './vars';

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Catamaran:700,900');

  *,
  *::before,
  *::after {
    position: relative;

    box-sizing: border-box;
  }

  html {
    font-family: Helvetica, Arial, sans-serif;
    font-size: 62.5%;
    text-size-adjust: 100%;

    &.fonts-loaded {
      font-family: 'Catamaran', sans-serif;
    }
  }

  body {
    overflow-x: hidden;
    overflow-y: scroll;
    margin: 0;

    transition: background-color 300ms;

    &.prim { background-color: ${colors.prim}; }
    &.sec { background-color: ${colors.sec}; }
  }
`;
