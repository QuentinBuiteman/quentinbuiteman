/* ==============================================================  /
   Combines
/  ============================================================== */

export const absStretch = () => (`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`);

export const center = () => (`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`);

export const clearfix = () => (`
  &::before,
  &::after {
    content: '';

    display: table;
  }

  &::after {
    clear: both;
  }
`);

export const valign = () => (`
  display: flex;
  align-items: center;
`);
