/* ==============================================================  /
   Colors
/  ============================================================== */

export const colors = {
  prim: '#e34d42',
  white: '#fff',
  sec: '#1e1e28',
};

/* ==============================================================  /
   Sizing
/  ============================================================== */

/* Screen sizes */

export const screens = {
  lStart: '59.375em',
  mEnd: '59.3125em',
  mStart: '40.0625em',
  sEnd: '40em',
  sStart: '28.75em',
  xsEnd: '28.6875em',
};

/* Font sizes */

const fontUnit = 1.6;

export const fonts = {
  unit: fontUnit,
  xs: `${0.625 * fontUnit}rem`,
  s: `${1 * fontUnit}rem`,
  m: `${1.25 * fontUnit}rem`,
  l: `${2 * fontUnit}rem`,
  xl: `${3.75 * fontUnit}rem`,
};

/* Spacing sizes */

const spacingUnit = 1.2;

export const spacing = {
  xs: `${0.5 * spacingUnit}rem`,
  s: `${1 * spacingUnit}rem`,
  m: `${2 * spacingUnit}rem`,
  l: `${3 * spacingUnit}rem`,
  xl: `${4 * spacingUnit}rem`,
};

/* Logo sizes */

export const logo = {
  l: '10.5rem',
  m: '8.5rem',
  s: '7rem',
};

/* Footer sizes */

export const footer = {
  l: '10rem',
  m: '5rem',
};
