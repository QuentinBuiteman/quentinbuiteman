import { keyframes } from 'styled-components';

export const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export const slideDown = keyframes`
  0% { transform: translateY(-100%); }
  100% { transform: translateY(0); }
`;

export const slideUp = keyframes`
  0% { transform: translateY(100%); }
  100% { transform: translateY(0); }
`;

export const position = keyframes`
  0% {
    position: absolute;
    top: 0;
    left: 0;

    width: 100%;
  }

  100% {
    position: relative;
    top: auto;
    left: auto;

    width: auto;
  }
`;
