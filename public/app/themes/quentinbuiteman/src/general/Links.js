import { storeHistory } from 'src/store';

/**
 * Handle hash locations
 */
export default class Links {
  /**
   * Construct new object from form element
   */
  constructor(elem) {
    /**
     * Add click listener to all links
     */
    this.links = elem.getElementsByTagName('a');
    this.length = this.links.length;
    this.addListeners();
  }

  /**
   * Loop over all links to bind scroll function
   */
  addListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];
      const href = link.getAttribute('href');

      if (href[0] === '#') {
        link.addEventListener('click', e => e.preventDefault(), false);
      } else if (!link.hasAttribute('target')) {
        link.addEventListener('click', Links.navigate, false);
      }
    }
  }

  /**
   * Loop over all links to remove scroll function
   */
  removeListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];
      const href = link.getAttribute('href');

      if (href[0] === '#' && href.length > 1) {
        link.removeEventListener('click', Links.scrollToHash, false);
      } else {
        link.removeEventListener('click', Links.navigatie, false);
      }
    }
  }

  /**
   * Navigate to new page
   *
   * @param e  object  Mouse event that's triggered after link click
   */
  static navigate(e) {
    e.preventDefault();

    // Get href and elem from href
    const href = e.currentTarget.getAttribute('href').replace(location.origin, '');

    // Navigate
    storeHistory.push(href);
  }
}
