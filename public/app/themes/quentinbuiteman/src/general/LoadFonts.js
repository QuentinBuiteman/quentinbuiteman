import FontFaceObserver from 'font-face-observer';

// Eliminate font render blocking
const LoadFonts = () => {
  if (document.documentElement.classList.contains('fonts-loaded')) {
    return;
  }

  const catamaran700 = new FontFaceObserver('Catamaran', {
    weight: 700,
  });
  const catamaran900 = new FontFaceObserver('Catamaran', {
    weight: 900,
  });

  Promise.all([
    catamaran700.check(),
    catamaran900.check(),
  ]).then(() => {
    document.documentElement.classList.add('fonts-loaded');
  });
};

export default LoadFonts;
