import fetch from 'isomorphic-fetch';

/**
 * Class to handle form submits.
 *
 * - Gets binded to form on render.
 * - Calls WordPress Ajax, which handles the validation and sending
 */
export default class SubmitForms {
  /**
   * Construct new object from form element
   */
  constructor(elem) {
    /**
     * Bound functions to keep this reference
     */
    this.boundValidate = e => this.validate(e);

    /**
     * Add submit listeners to all forms
     */
    this.forms = elem.querySelectorAll('form');
    this.formsLength = this.forms.length;
    this.addListeners();
  }

  /**
   * Loop over all forms to add validate function to submit
   */
  addListeners() {
    let i = 0;

    for (i; i < this.formsLength; i += 1) {
      const form = this.forms[i];

      form.addEventListener('submit', this.boundValidate, false);
    }
  }

  /**
   * Loop over all forms to remove validate function to submit
   */
  removeListeners() {
    let i = 0;

    for (i; i < this.formsLength; i += 1) {
      const form = this.forms[i];

      form.removeEventListener('submit', this.boundValidate, false);
    }
  }

  /**
   * Quick validation to check if required fields are not left empty
   */
  validate(e) {
    // Prevent sending form
    e.preventDefault();

    // Get form elem
    const elem = e.target;

    // Build object
    const form = {};
    form.elem = elem;
    form.fields = elem.querySelector('.form__fields');
    form.inputs = elem.querySelectorAll('.field__input');
    form.submit = elem.querySelector('.form__submit');
    form.loader = elem.querySelector('.form__loader');

    // Variable to track checks
    let check = true;

    // Loop over inputs
    let i = 0;
    const length = form.inputs.length;

    for (i; i < length; i += 1) {
      const input = form.inputs[i];
      const error = input.parentNode.querySelector('.field__error');

      // Set check to false if input is not valid
      if (typeof input.willValidate !== 'undefined') {
        if (!input.checkValidity()) {
          check = false;

          // Show error message
          if (error) {
            error.style.opacity = 1;
          }
        } else if (error) {
          error.style.opacity = 0;
        }
      }
    }

    // Send form if check is passed
    if (check) {
      this.send(form);
    }
  }

  /**
   * Function to send the form data
   */
  send(form) {
    const wpAjax = form.elem.getAttribute('action');
    const action = form.elem.getAttribute('data-action');
    const formId = form.elem.getAttribute('data-id');
    const url = `${wpAjax}?action=${action}`;

    // Disable submit and show loading icon
    form.fields.classList.add('disabled');
    form.submit.classList.add('disabled');
    form.loader.classList.add('show');

      // Set formdata object
    const formData = new FormData(form.elem);
    formData.append('form_id', formId);

    // Make ajax call
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then((json) => {
        this.complete(form, json);
      });
  }

  /**
   * Handle response
   *
   * @param string response
   */
  complete(form, json) {
    // Hide loader
    form.loader.classList.remove('show');

    // Json to this
    this.json = json;

    // Succes
    if (json.check) {
      form.submit.querySelector('.btn').setAttribute('disabled', 'disabled');
      form.elem.reset();
    } else {
      form.fields.classList.remove('disabled');
      form.submit.classList.remove('disabled');
    }
  }
}
