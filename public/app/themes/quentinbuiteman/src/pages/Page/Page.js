// Libraries
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// JavaScript
import Mount from 'pages/Mount';
import Unmount from 'pages/Unmount';

// Components
import Fields from 'components/Fields/Fields';
import Person from 'components/Person/Person';

// Styling
import Wrapper from './styling/Wrapper';

// Build Page component
class Page extends PureComponent {
  /**
   * Run general combined mount function
   */
  componentDidMount() {
    Mount(this, this.div);
  }

  /**
   * Run general combined unmount function
   */
  componentWillUnmount() {
    Unmount(this);
  }

  /**
   * Render component
   *
   * @return HTML
   */
  render() {
    const fields = this.props.fields;
    const img = this.props.img;

    return (
      <Wrapper innerRef={(div) => { this.div = div; }}>
        <Person img={img} />

        <Fields fields={fields} />
      </Wrapper>
    );
  }
}

// Set proptypes
Page.propTypes = {
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
  img: PropTypes.string.isRequired,
};

export default Page;
