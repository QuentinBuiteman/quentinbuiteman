/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { clearfix } from 'general/styling/mixins';

/* ==============================================================  /
   Container
/  ============================================================== */

const Wrapper = styled.div`
  ${clearfix}
`;

export default Wrapper;
