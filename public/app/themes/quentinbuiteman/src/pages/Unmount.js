/**
 * Delete all event listeners from mount function
 */
const Unmount = (comp) => {
  // Remove event listeners on links
  comp.links.removeListeners();

  // Remove event listeners on forms
  comp.forms.removeListeners();
};

export default Unmount;
