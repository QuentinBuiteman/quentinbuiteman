// JavaScript
import Links from 'general/Links';
import SubmitForms from 'general/SubmitForms';

/**
 * Combine function for all standard mounting functions
 *
 * Gets run on ComponentDidMount() on different page type components
 *
 * - Adds event listeners to link clicks
 * - Adds event listeners to form submits
 *
 * @property comp  object  React component on which functions should run
 * @property el    object  Reference to element to use for queryselectors
 */
const Mount = (comp, el) => {
  // Add event listeners to links
  comp.links = new Links(el);

  // Add event listeners to forms
  comp.forms = new SubmitForms(el);

  // Scroll to top of page
  setTimeout(() => window.scrollTo(0, 0), 220);
};

export default Mount;
