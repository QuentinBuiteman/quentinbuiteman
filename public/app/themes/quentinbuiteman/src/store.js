// Libraries
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux';

// Import reducers and create store
import reducers from './reducers';

// Create an enhanced history that syncs navigation events with the store
const storeHistory = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(storeHistory);

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer,
  }),
  applyMiddleware(middleware, thunk),
);

export { storeHistory, store };
