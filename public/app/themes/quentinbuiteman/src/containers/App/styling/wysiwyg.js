/* ==============================================================  /
   Dependencies
/  ============================================================== */

import { css } from 'styled-components';

import { colors, fonts, screens, spacing } from 'general/styling/vars';

/* ==============================================================  /
   Editor classes
/  ============================================================== */

const wysiwyg = css`
  /* Colors */

  .clr--white { color: ${colors.white}; }
  .clr--prim { color: ${colors.prim}; }
  .clr--sec { color: ${colors.sec}; }

  /* Font sizes */

  .fs-xl {
    @media screen and (min-width: ${screens.mStart}) { font-size: ${fonts.xl}; }

    @media screen and (max-width: ${screens.sEnd}) { font-size: ${2.5 * fonts.unit}rem; }
  }

  .fs-l {
    @media screen and (min-width: ${screens.mStart}) { font-size: ${fonts.l}; }

    @media screen and (max-width: ${screens.sEnd}) { font-size: ${fonts.m}; }
  }

  .fs-m {
    @media screen and (min-width: ${screens.mStart}) { font-size: ${fonts.m}; }

    @media screen and (max-width: ${screens.sEnd}) { font-size: ${fonts.s}; }
  }

  .fs-s {
    @media screen and (min-width: ${screens.mStart}) { font-size: ${fonts.s}; }

    @media screen and (max-width: ${screens.sEnd}) { font-size: ${fonts.xs}; }
  }

  /* Button */

  .btn {
    display: inline-block;
    padding: 1.4rem ${spacing.m};
    border: 0;
    outline: 0;
    margin: 0;

    color: ${colors.white};
    background-color: ${props => props.theme.fg};

    font-size: ${fonts.m};
    line-height: ${spacing.m};
    text-decoration: none;

    transition: background-color 300ms, color 300ms;
  }
`;

export default wysiwyg;
