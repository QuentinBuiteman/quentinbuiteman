/* ==============================================================  /
   Dependencies
/  ============================================================== */

import { css } from 'styled-components';

import { colors, fonts, screens, spacing } from 'general/styling/vars';

/* ==============================================================  /
   Global styling
/  ============================================================== */

const global = css`
  main {
    overflow: hidden;
  }

  h1,
  h2,
  p,
  form {
    width: 100%;
    max-width: 74rem;
    padding-left: ${spacing.s};
    padding-right: ${spacing.s};
    margin-top: 0;
    margin-left: auto;
    margin-right: auto;

    direction: ltr;

    @media screen and (max-width: ${screens.mEnd}) {
      text-align: center;
    }
  }

  h1,
  h2 {
    color: ${props => props.theme.fg};

    font-weight: 900;
    line-height: 1.2;
  }

  p {
    color: ${colors.white};

    font-weight: 700;
    line-height: 1.7;

    a {
      color: ${props => props.theme.fg};

      &:hover { color: $white-clr; }
    }
  }

  /* Remove browser-specific focus, active and hover styles */

  a:focus,
  a:active,
  a:hover {
    outline: 0;
  }

  /* Correct styles for form elements */

  button,
  input,
  select,
  textarea {
    font-family: inherit;
  }

  textarea {
    vertical-align: top;
    overflow: hidden;
    height: 15rem;

    resize: none;
  }

  [type='search'],
  [type='submit'] {
    -webkit-appearance: none;
  }

  [type='submit'] {
    cursor: pointer;
  }

  /* ==============================================================  /
     Text
  /  ============================================================== */

  h1,
  h2,
  p {
    margin-bottom: ${spacing.l};
  }

  h1:last-child,
  h2:last-child,
  p:last-child {
    margin-bottom: 0;
  }

  p,
  a {
    @media screen and (min-width: ${screens.mStart}) { font-size: ${fonts.m}; }

    @media screen and (max-width: ${screens.sEnd}) { font-size: ${fonts.s}; }
  }

  a {
    text-decoration: none;

    transition: color 300ms;
  }
`;

export default global;
