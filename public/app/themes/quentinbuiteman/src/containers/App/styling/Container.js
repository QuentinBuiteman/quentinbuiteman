/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { footer, logo, screens } from 'general/styling/vars';

import global from './global';
import wysiwyg from './wysiwyg';
import form from './form';

/* ==============================================================  /
   Container
/  ============================================================== */

const Container = styled.div`
  @media screen and (min-width: ${screens.lStart}) {
    padding-bottom: ${footer.l};
    margin-top: ${logo.l};
  }

  @media screen and (max-width: ${screens.mEnd}) and (min-width: ${screens.sStart}) { padding-bottom: ${footer.m}; }

  @media screen and (max-width: ${screens.xsEnd}) { padding-bottom: ${footer.m}; }

  ${global}
  ${wysiwyg}
  ${form}
`;

export default Container;
