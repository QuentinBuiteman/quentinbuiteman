/* ==============================================================  /
   Dependencies
/  ============================================================== */

import { css } from 'styled-components';

import { colors, fonts, screens, spacing } from 'general/styling/vars';
import { absStretch, clearfix } from 'general/styling/mixins';
import { spin } from 'general/styling/animations';

/* ==============================================================  /
   Global styling
/  ============================================================== */

const form = css`
  /* Form basics */

  .form {
    &__fields {
      ${clearfix}

      margin-right: -${spacing.s};
      margin-left: -${spacing.s};
    }

    &__foot {
      padding: ${spacing.m} ${spacing.s} 0;
      margin-left: -${spacing.s};
      margin-right: -${spacing.s};

      @media screen and (min-width: ${screens.mStart}) {
        text-align: right;
      }

      @media screen and (max-width: ${screens.sEnd}) {
        text-align: center;
      }
    }

    &__submit {
      display: inline-block;

      &.disabled {
        [type='submit'] { color: rgba(0, 0, 0, 0) !important; }

        &::after {
          content: '';

          ${absStretch}

          z-index: 5;

          opacity: 0;
        }
      }

      [type='submit'] {
        z-index: 1;

        display: inline-block;

        &:disabled {
          color: rgba(0, 0, 0, 0) !important;

          cursor: default;

          + .form__check {
            z-index: 2;

            opacity: 1;
          }
        }
      }
    }
  }

  /* Fields */

  .form .field {
    float: left;
    padding: 0 ${spacing.s} ${spacing.m};

    &.textarea { width: 100%; }

    @media screen and (min-width: ${screens.mStart}) { width: 50%; }

    @media screen and (max-width: ${screens.sEnd}) { width: 100%; }

    &__input {
      vertical-align: middle;
      width: 100%;
      padding: 1.1rem 1.5rem;
      border: 0.2rem solid #fff;
      margin: 0;

      color: ${colors.sec};
      background-color: ${colors.white};

      font-size: ${fonts.s};
      line-height: ${spacing.m};
      text-align: left;

      transition: border-color 300ms;

      &:valid {
        border-color: green;

        + .field__error {
          opacity: 0 !important;
        }
      }

      &:focus {
        outline: 0;

        &::-webkit-input-placeholder { color: transparent; }
        &:-moz-placeholder { color: transparent; }

        + .field__error + .field__label {
          opacity: 1;
        }
      }

      &:focus:invalid {
        border-color: ${colors.prim};

        + .field__error {
          opacity: 1;
        }
      }

      &:not(textarea) {
        height: 4.8rem;
      }
    }

    &__error {
      position: absolute;
      right: 2.7rem;
      bottom: 0;
      left: 2.7rem;

      display: inline-block;
      width: auto;
      padding: 0;
      margin: 0;

      color: ${colors.prim};
      opacity: 0;

      font-size: ${fonts.xs};
      line-height: ${spacing.m};

      transition: opacity 300ms;
    }

    &__label {
      position: absolute;
      z-index: 2;
      top: -${spacing.m};
      left: 2.7rem;

      display: block;

      color: ${colors.white};
      background-color: transparent;
      opacity: 0;

      font-size: ${fonts.xs};
      line-height: ${spacing.m};
      text-align: left;

      transition: opacity 300ms;

      cursor: text;
      user-select: none;
    }
  }

  /* Processing */

  .form__check {
    position: absolute;
    z-index: 0;
    top: 45%;
    left: 50%;
    transform: translate(-50%, -50%);

    display: block;

    opacity: 0;

    transition: opacity 300ms;

    &::after {
      content: '';

      transform: rotate(40deg);

      display: block;
      width: ${spacing.s};
      height: ${spacing.m};
      border-style: solid;
      border-width: 0 0.4rem 0.4rem 0;
      border-color: ${colors.white};
    }
  }

  .form__loader {
    position: absolute;
    z-index: 0;
    top: calc(50% - ${spacing.s});
    left: calc(50% - ${spacing.s});

    display: block;
    width: ${spacing.m};
    height: ${spacing.m};
    border-radius: 50%;
    border-width: 0.3rem;
    border-style: solid;
    border-color: rgba(255, 255, 255, 0.2);
    border-left-color: ${colors.white};

    opacity: 0;

    transition: opacity 300ms, z-index 0s 300ms;

    animation: ${spin} 1100ms infinite linear;

    &.show {
      z-index: 1;

      opacity: 1;

      transition: opacity 300ms, z-index 0s 0s;
    }
  }
`;

export default form;
