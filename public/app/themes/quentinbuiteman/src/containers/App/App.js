// Libraries
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

// Styling
import 'general/styling/global';
import { colors } from 'general/styling/vars';
import Container from './styling/Container';

// Import components
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import Routes from './components/Routes/Routes';
import Footer from './components/Footer/Footer';

// Initialize theme
let theme = {};

/**
 * Build content component
 *
 * @property  fetchMenu      boolean  Check for AJAX request for app content
 * @property  fetchContent  boolean  Check for AJAX request for page content
 *
 * @return HTML  Div with Header, Routes, Footer
 */
const App = ({ clr, fetchMenu, fetchContent }) => {
  /**
   * Set colors if value is set
   */
  if (clr) {
    /* Change background color */
    document.body.setAttribute('class', clr);

    /* Set theme colors */
    theme = {
      bg: (clr === 'prim') ? colors.prim : colors.sec,
      fg: (clr === 'prim') ? colors.sec : colors.prim,
      txt: (clr === 'prim') ? colors.sec : colors.white,
    };
  }

  return (
    <ThemeProvider theme={theme}>
      <Container>
        { !fetchMenu && <Header /> }

        <Loader fetch={fetchContent} />

        <Routes />

        { !fetchMenu && <Footer /> }
      </Container>
    </ThemeProvider>
  );
};

// PropTypes check
App.propTypes = {
  clr: PropTypes.string.isRequired,
  fetchMenu: PropTypes.bool.isRequired,
  fetchContent: PropTypes.bool.isRequired,
};

// Mape state to prop
const mapStateToProps = state => ({
  clr: state.content.clr,
  fetchMenu: state.app.fetch,
  fetchContent: state.content.fetch,
});

// Connect and export
export default connect(mapStateToProps)(App);
