// Imports
import fetch from 'isomorphic-fetch';

// Action types
export const REQUEST_MENU = 'REQUEST_MENU';
export const RECEIVE_MENU = 'RECEIVE_MENU';
export const FAIL_MENU = 'FAIL_MENU';

// Action creator for request menu
export const requestMenu = () => ({
  type: REQUEST_MENU,
});

// Action creator for receive menu
export const receiveMenu = data => ({
  type: RECEIVE_MENU,
  menu: data,
});

// Action creator for fail of menu request
export const failMenu = () => ({
  type: FAIL_MENU,
});

// Action to fetch the menu from API
export const fetchMenu = () => (
  (dispatch) => {
    // Update app state to know fetch has started
    dispatch(requestMenu());

    // Rest api url
    const url = '/wp-json/qb/v2/menu/primary';

    // Fetch from url
    return fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((json) => {
        if (typeof json.errors !== 'undefined' || typeof json.error !== 'undefined') {
          throw new Error('Errors occurred while calling the API');
        }

        dispatch(receiveMenu(json));
      })

      // Catch error and log
      .catch((exception) => {
        dispatch(failMenu());
        throw exception;
      });
  }
);
