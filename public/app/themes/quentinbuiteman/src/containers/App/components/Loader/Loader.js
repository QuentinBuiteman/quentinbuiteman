// Libraries
import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import PropTypes from 'prop-types';

// Styling
import StyledLoader from './styling/StyledLoader';

/**
 * Build Main component
 *
 * @property  fetch  boolean  Check for AJAX request for page content
 */
const Loader = ({ fetch }) => (
  <CSSTransitionGroup
    component={StyledLoader}
    transitionName="loader"
    transitionEnterTimeout={200}
    transitionLeaveTimeout={200}
  >
    { fetch && <div /> }
  </CSSTransitionGroup>
);

// PropTypes check
Loader.propTypes = {
  fetch: PropTypes.bool.isRequired,
};

export default Loader;
