// Imports
import fetch from 'isomorphic-fetch';

// Action types
export const REQUEST_CONTENT = 'REQUEST_CONTENT';
export const RECEIVE_CONTENT = 'RECEIVE_CONTENT';
export const FAIL_CONTENT = 'FAIL_CONTENT';

// Action creator for request menu
export const requestContent = () => ({
  type: REQUEST_CONTENT,
});

// Action creator for receive menu
export const receiveContent = data => ({
  type: RECEIVE_CONTENT,
  fields: data.fields,
  img: data.img,
  clr: data.clr,
  title: data.title,
});

// Action creator for fail of menu request
export const failContent = () => ({
  type: FAIL_CONTENT,
});

// Action to fetch the menu from API
export const fetchContent = pathname => (
  (dispatch) => {
    // Update app state to know fetch has started
    dispatch(requestContent());

    // Get pagename from argument and remove first and last slash
    let path = (pathname === '/') ? '/home/' : pathname;
    path = path.replace(/^\/|\/$/g, '');
    path = path.replace(/\//g, '--');

    // Rest api url
    const url = `/wp-json/qb/v2/page/${path}`;

    // Fetch from url
    return fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((json) => {
        if (typeof json.errors !== 'undefined' || typeof json.error !== 'undefined') {
          throw new Error('Errors occurred while calling the API');
        }

        // Update Analytics
        if (typeof window.ga !== 'undefined') {
          window.ga('set', 'page', pathname);
          window.ga('send', 'pageview');
        }

        dispatch(receiveContent(json));
      })

      // Catch error and log
      .catch((exception) => {
        dispatch(failContent());
        throw exception;
      });
  }
);
