/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { fonts, screens, spacing } from 'general/styling/vars';

/* ==============================================================  /
   Site Menu
/  ============================================================== */

const Menu = styled.nav`
  display: inline-block;

  ul {
    display: flex;
    margin: 0;

    list-style: none;

    @media screen and (min-width: ${screens.lStart}) {
      padding: ${spacing.m} ${spacing.m} 0 0;
    }

    @media screen and (max-width: ${screens.mEnd}) {
      padding: 0;
    }
  }

  a {
    display: block;

    color: inherit;

    text-decoration: none;
    white-space: nowrap;

    transition: color 300ms;

    &.active,
    &:hover {
      color: ${props => props.theme.bg};
    }

    @media screen and (min-width: ${screens.lStart}) {
      padding: 0 ${spacing.m};

      font-size: ${fonts.m};
      line-height: 8rem;
    }

    @media screen and (max-width: ${screens.mEnd}) {
      padding: 0 1.5rem;

      font-size: ${fonts.s};
      line-height: 5rem;
    }
  }
`;

export default Menu;
