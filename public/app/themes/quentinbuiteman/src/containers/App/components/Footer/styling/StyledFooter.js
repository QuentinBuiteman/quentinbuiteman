/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { colors, footer, screens } from 'general/styling/vars';
import { slideUp } from 'general/styling/animations';

/* ==============================================================  /
   Site footer
/  ============================================================== */

const StyledFooter = styled.footer`
  position: fixed;
  z-index: 5;
  bottom: 0;

  width: 100%;

  color: ${colors.white};

  transition: background-color 300ms;

  animation: ${slideUp} 300ms ease-in-out 0s 1;

  /* Height adjustment for responsive */

  @media screen and (min-width: ${screens.lStart}) {
    height: ${footer.l};

    text-align: right;
  }

  @media screen and (max-width: ${screens.mEnd}) {
    height: ${footer.m};

    background-color: ${props => props.theme.fg};

    text-align: center;
  }
`;

export default StyledFooter;
