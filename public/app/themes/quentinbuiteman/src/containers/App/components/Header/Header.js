// Imports
import React from 'react';

// Import Logo SVG
import Logo from 'img/logo.svg';

// Styling
import StyledHeader from './styling/StyledHeader';
import StyledLogo from './styling/StyledLogo';

// Build Header component
const Header = () => (
  <StyledHeader role="banner">
    <StyledLogo to="/">
      <Logo />
    </StyledLogo>
  </StyledHeader>
);

export default Header;
