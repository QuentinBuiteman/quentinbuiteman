/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { logo, screens, spacing } from 'general/styling/vars';
import { slideDown } from 'general/styling/animations';

/* ==============================================================  /
   Logo
/  ============================================================== */

const StyledLogo = styled(Link)`
  display: inline-block;

  animation: ${slideDown} 300ms ease-in-out 0s 1;

  @media screen and (min-width: ${screens.lStart}) {
    height: ${logo.l};
    padding: ${spacing.m};
  }

  @media screen and (max-width: ${screens.mEnd}) and (min-width: ${screens.sStart}) {
    height: ${logo.m};
    padding: ${spacing.s};
  }

  @media screen and (max-width: ${screens.xsEnd}) {
    height: ${logo.s};
    padding: ${spacing.s};
  }

  svg {
    display: block;
    width: auto;
    height: 100%;
  }

  path {
    transition: fill 300ms;

    &:first-child,
    &:last-child {
      fill: ${props => props.theme.fg};
    }
  }
`;

export default StyledLogo;
