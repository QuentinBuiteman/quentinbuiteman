/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

// Variables
import { screens, spacing } from 'general/styling/vars';

// Import Line SVG
import Line from 'img/line.svg';

/* ==============================================================  /
   Styled line
/  ============================================================== */

const StyledLine = styled(Line)`
  position: absolute;
  top: 0;
  left: 0;

  display: block;
  width: 100%;

  path {
    display: block;
    width: 100%;
    height: 100%;

    fill: ${props => props.theme.fg};

    transition: fill 300ms;
  }

  @media screen and (min-width: ${screens.lStart}) {
    height: 100%;
  }

  @media screen and (max-width: ${screens.mEnd}) and (min-width: ${screens.sStart}) {
    transform: translateY(-100%);

    height: ${spacing.l};
  }

  @media screen and (max-width: $screen-xs-end) {
    transform: translateY(-100%);

    height: ${spacing.m};
  }
`;

export default StyledLine;
