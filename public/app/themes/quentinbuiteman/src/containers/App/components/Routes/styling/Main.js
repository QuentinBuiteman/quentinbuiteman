/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { position } from 'general/styling/animations';

/* ==============================================================  /
   Site header
/  ============================================================== */

const Main = styled.main`
  /* Fade in */

  .main-enter {
    animation: ${position} 0s ease-out 300ms 1;
    animation-fill-mode: both;

    .content {
      opacity: 0.01;
    }

    .person svg {
      bottom: -100%;
    }

    &.main-enter-active {
      .content {
        opacity: 1;

        transition: opacity 300ms 300ms ease-in;
      }

      .person svg {
        bottom: 0;

        transition: bottom 300ms 300ms ease-out;
      }
    }
  }

  /* Fade out */

  .main-leave {
    .content {
      opacity: 1;
    }

    .person svg {
      bottom: 0;
    }

    &.main-leave-active {
      .content {
        opacity: 0.01;

        transition: opacity 300ms ease-out;
      }

      .person svg {
        bottom: -100%;

        transition: bottom 300ms ease-in;
      }
    }
  }
`;

export default Main;
