import { REQUEST_CONTENT, RECEIVE_CONTENT, FAIL_CONTENT } from './RoutesActions';

/**
 * The ContentReducer which handles all the content state changes
 *
 * @param state
 * @param action
 *
 * @return {object}
 */
const ContentReducer = (state = {
  fetch: true,
  error: false,
  fields: [],
  img: '',
  clr: '',
  title: null,
}, action) => {
  switch (action.type) {
    // Request of menu
    case REQUEST_CONTENT:
      return Object.assign({}, state, {
        fetch: true,
      });

    // When menu is received
    case RECEIVE_CONTENT:
      return Object.assign({}, state, {
        fetch: false,
        error: false,
        fields: (action.fields) ? action.fields : state.fields,
        img: (action.img) ? action.img : state.img,
        clr: (action.clr) ? action.clr : state.clr,
        title: (action.title) ? action.title : state.title,
      });

    // On fail of receival
    case FAIL_CONTENT:
      return Object.assign({}, state, {
        fetch: false,
        error: true,
      });

    // Default
    default:
      return state;
  }
};

export default ContentReducer;
