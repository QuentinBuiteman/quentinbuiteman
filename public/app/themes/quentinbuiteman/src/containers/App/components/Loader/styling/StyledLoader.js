/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { spacing } from 'general/styling/vars';
import { spin } from 'general/styling/animations';

/* ==============================================================  /
   Loader
/  ============================================================== */

const StyledLoader = styled.div`
  /* Fade in */

  .loader-enter {
    opacity: 0.01;

    &.loader-enter-active {
      opacity: 1;

      transition: opacity 200ms ease-in-out;
    }
  }

  /* Fade out */

  .loader-leave {
    opacity: 1;

    &.loader-leave-active {
      opacity: 0.01;

      transition: opacity 200ms ease-in-out;
    }
  }

  /* Icon */

  div {
    position: fixed;
    z-index: 50;
    top: calc(50% - ${spacing.m});
    left: calc(50% - ${spacing.m});

    width: ${spacing.xl};
    height: ${spacing.xl};

    animation: ${spin} 1100ms infinite linear;

    &::before,
    &::after {
      content: '';

      position: absolute;
      top: 0;
      left: 0;

      width: 100%;
      height: 100%;
      border-radius: 50%;
      border-width: ${spacing.xs};
      border-style: solid;
    }

    &::before {
      border-color: #fff;

      opacity: 0.2;
    }

    &::after {
      border-color: transparent;
      border-left-color: #fff;
    }
  }
`;

export default StyledLoader;
