// Libraries
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Route, Switch } from 'react-router-dom';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import PropTypes from 'prop-types';

// Components
import Page from 'pages/Page/Page';

// Styling
import Main from './styling/Main';

/**
 * Build Routes component
 *
 * @property  fetch     bool     If content is being fetched
 * @property  fields    array    The page content
 * @property  img       string   The page image
 * @property  location  object   Location object
 * @property  title     string   Title of the page
 *
 * @return HTML  Rendered route or Loader
 */
const Routes = ({ fetch, fields, img, location, title }) => {
  /**
   * Set browser title
   */
  if (title) {
    document.title = title;
  }

  /**
   * Return transition group and routes
   */
  return (
    <CSSTransitionGroup
      component={Main}
      transitionName="main"
      transitionEnterTimeout={600}
      transitionLeaveTimeout={300}
    >
      { !fetch &&
        <Switch key={location.key} location={location}>
          <Route
            render={() => (
              <Page fields={fields} img={img} />
            )}
          />
        </Switch>
      }
    </CSSTransitionGroup>
  );
};

// Default values for props
Routes.defaultProps = {
  title: '',
};

// PropTypes check
Routes.propTypes = {
  fetch: PropTypes.bool.isRequired,
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
  img: PropTypes.string.isRequired,
  location: PropTypes.shape({}).isRequired,
  title: PropTypes.string,
};

// Mape state to prop
const mapStateToProps = state => ({
  fetch: state.content.fetch,
  fields: state.content.fields,
  img: state.content.img,
  title: state.content.title,
});

// Connect and export
export default withRouter(connect(mapStateToProps)(Routes));
