/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { screens } from 'general/styling/vars';

/* ==============================================================  /
   Site header
/  ============================================================== */

const StyledHeader = styled.header`
  z-index: 10;

  width: 100%;

  font-size: 0;
  text-align: center;

  transition: background-color 300ms;

  @media screen and (min-width: ${screens.lStart}) {
    position: fixed;
    top: 0;
    left: 0;

    background-color: ${props => props.theme.bg};
  }
`;

export default StyledHeader;
