// Imports
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

// Styling
import StyledFooter from './styling/StyledFooter';
import StyledLine from './styling/StyledLine';
import Menu from './styling/Menu';

// Build Footer component
const Footer = ({ menu }) => (
  <StyledFooter>
    <StyledLine />

    <Menu role="navigation">
      <ul>
        { menu.items.map(item => (
          <li key={item.id}>
            <NavLink exact to={item.url} activeClassName="active">{item.title}</NavLink>
          </li>
        )) }
      </ul>
    </Menu>
  </StyledFooter>
);

// PropTypes check
Footer.propTypes = {
  menu: PropTypes.shape({}).isRequired,
};

// Mape state to prop
const mapStateToProps = state => ({
  menu: state.app.menu,
});

// Connect and export
export default withRouter(connect(mapStateToProps)(Footer));
