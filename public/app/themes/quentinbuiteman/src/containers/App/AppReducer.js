import { REQUEST_MENU, RECEIVE_MENU, FAIL_MENU } from './AppActions';

/**
 * The FooterReducer which handles all the footer state changes
 *
 * @param state
 * @param action
 *
 * @return {object}
 */
const AppReducer = (state = {
  fetch: true,
  hasError: false,
  menu: {},
}, action) => {
  switch (action.type) {
    // Request of menu
    case REQUEST_MENU:
      return Object.assign({}, state, {
        fetch: true,
      });

    // When menu is received
    case RECEIVE_MENU:
      return Object.assign({}, state, {
        fetch: false,
        hasError: false,
        menu: (action.menu) ? action.menu : state.menu,
      });

    // On fail of receival
    case FAIL_MENU:
      return Object.assign({}, state, {
        fetch: false,
        hasError: true,
      });

    // Default
    default:
      return state;
  }
};

export default AppReducer;
