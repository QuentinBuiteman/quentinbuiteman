// Imports
import React from 'react';
import PropTypes from 'prop-types';

// Styling
import StyledPerson from './styling/StyledPerson';

// Build Person component
const Person = ({ img }) => (
  <StyledPerson className="person">
    <figure dangerouslySetInnerHTML={{ __html: img }} />
  </StyledPerson>
);

// Set proptypes
Person.propTypes = {
  img: PropTypes.string.isRequired,
};

export default Person;
