/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { screens } from 'src/general/styling/vars';

/* ==============================================================  /
   Site StyledPerson
/  ============================================================== */

const StyledPerson = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;

  height: 100%;
  margin: 0;

  @media screen and (min-width: ${screens.mStart}) { width: 50%; }

  @media screen and (max-width: ${screens.sEnd}) and (min-width: ${screens.sStart}) { width: 75%; }

  @media screen and (max-width: ${screens.xsEnd}) { width: 100%; }

  figure {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
  }

  svg {
    position: absolute;
    bottom: 0;
    transform: translateX(-50%);

    @media screen and (min-width: ${screens.lStart}) {
      left: 50%;
    }

    @media screen and (max-width: ${screens.mEnd}) {
      left: 10%;

      opacity: 0.1;
    }

    display: inline-block;
    width: 80%;
    height: auto;
    max-height: 75%;
  }
`;

export default StyledPerson;
