/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { footer, logo, screens, spacing } from 'src/general/styling/vars';

/* ==============================================================  /
   Flex
/  ============================================================== */

const Flex = styled.section`
  display: flex;
  justify-content: center;
  align-items: stretch;

  > * {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    width: 100%;
  }
`;

export const OuterFlex = Flex.extend`
  @media screen and (min-width: ${screens.lStart}) {
    float: right;
    width: 50%;
  }

  @media screen and (max-width: ${screens.mEnd}) {
    width: 100%;
  }

  > * {
    padding: ${spacing.s};

    @media screen and (min-width: ${screens.lStart}) {
      min-height: calc(100vh - ${logo.l} - ${footer.l});
    }

    @media screen and (max-width: ${screens.mEnd}) and (min-width: ${screens.sStart}) {
      min-height: calc(100vh - ${logo.m} - ${footer.m});
    }

    @media screen and (max-width: ${screens.xsEnd}) {
      min-height: calc(100vh - ${logo.s} - ${footer.m});
    }

    > * {
      width: 100%;

      > * {
        width: 100%;
        padding: ${spacing.m} 0;
      }
    }
  }
`;

export const InnerFlex = Flex.extend`
  max-width: 74rem;
  margin-left: auto;
  margin-right: auto;

  > * {
    > * {
      @media screen and (min-width: ${screens.sStart}) { width: 25%; }

      @media screen and (max-width: ${screens.xsEnd}) { width: 50%; }

      padding-left: ${spacing.s};
      padding-right: ${spacing.s};
    }
  }
`;
