// Imports
import React from 'react';
import PropTypes from 'prop-types';

// Styling
import { InnerFlex } from './../../styling/Flex';
import Icon from './styling/Icon';

// Build Header component
const Images = ({ images }) => (
  <InnerFlex>
    <div>
      { images.map((image, i) => (
        <div key={i.toString()}>
          <Icon>
            <div dangerouslySetInnerHTML={{ __html: image.img }} />

            <span>{ image.name }</span>
          </Icon>
        </div>
      )) }
    </div>
  </InnerFlex>
);

// Set proptypes
Images.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Images;
