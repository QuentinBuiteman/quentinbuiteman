/* ==============================================================  /
   Dependencies
/  ============================================================== */

import styled from 'styled-components';

import { colors, fonts, spacing } from 'src/general/styling/vars';

/* ==============================================================  /
   Icon
/  ============================================================== */

const Icon = styled.div`
  width: 10rem;
  max-width: 100%;
  margin: ${spacing.m} auto;

  > div {
    display: flex;
    align-items: center;
    width: 100%;
    height: 0;
    padding-bottom: 100%;
  }

  svg {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    width: 55%;
    height: 55%;
  }

  &::before {
    content: '';

    position: absolute;
    top: 0;
    left: 0;
    transform: rotate(45deg);

    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;

    background-color: rgba(255, 255, 255, 0.1);
  }

  span {
    position: absolute;
    z-index: 0;
    top: -3.7rem;
    left: 50%;
    transform: translateX(-50%);

    display: block;
    padding: 0 ${spacing.s};
    border-radius: 0.3rem;

    color: ${colors.white};
    background-color: ${colors.sec};
    opacity: 0;

    font-size: ${fonts.s};
    line-height: ${spacing.l};
    text-align: center;
    white-space: nowrap;

    transition: opacity 300ms, z-index 0s 300ms;

    &::before {
      content: '';

      position: absolute;
      top: 100%;
      left: 50%;
      transform: rotate(45deg);
      transform-origin: center;

      width: ${spacing.s};
      height: ${spacing.s};
      border-radius: 0.2rem;
      margin-left: -${spacing.xs};
      margin-top: -${spacing.xs};

      background-color: ${colors.sec};
    }
  }

  &:hover span {
    z-index: 1;

    opacity: 1;

    transition: opacity 300ms, z-index 0s 0s;
  }
`;

export default Icon;
