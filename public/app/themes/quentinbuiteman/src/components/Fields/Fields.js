// Imports
import React from 'react';
import PropTypes from 'prop-types';

// Styling
import { OuterFlex } from './styling/Flex';

// Import Images component
import Images from './components/Images/Images';

// Build Fields component
const Fields = ({ fields }) => {
  // Loop over all fields
  const html = fields.map((field, i) => {
    let elem = null;

    switch (field.acf_fc_layout) {
      default:
        break;

      case 'field_content':
        elem = <div key={i.toString()} dangerouslySetInnerHTML={{ __html: field.content }} />;

        break;

      case 'field_images': {
        const images = field.images;

        if (images.length > 0) {
          elem = <Images key={i.toString()} images={images} />;
        }

        break;
      }
    }

    return (elem);
  });

  // Return div with ref and fields as innerHTML
  return (
    <OuterFlex className="content">
      <article>
        <div>
          { html }
        </div>
      </article>
    </OuterFlex>
  );
};

// Set proptypes
Fields.propTypes = {
  fields: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Fields;
