// Import reducers
import AppReducer from 'containers/App/AppReducer';
import ContentReducer from 'containers/App/components/Routes/RoutesReducer';

// Create reducers
const reducers = {
  app: AppReducer,
  content: ContentReducer,
};

export default reducers;
