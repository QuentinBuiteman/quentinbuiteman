<?php

namespace QB\Ajax;

/**
 * Handling data from submitting of forms
 */
class SendForm implements \QB\PluginAPI\ActionHook
{
    /**
     * @var string   $response     String with response
     * @var QDOM     $DOM          DOM to build HTML message
     * @var int      $form_id      ID of the form
     * @var boolean  $check        Check if mail should be sent
     * @var array    $attachments  Array of attachments
     */
    public $response;
    private $DOM;
    private $form_id;
    private $check = false;

    /**
     * Subscribe functions to corresponding actions
     *
     * @return array  Consists of 'action_to_subscribe_to' => 'function_to_run'
     */
    public static function getActions(): array
    {
        return array(
            'wp_ajax_nopriv_send_form' => 'sendForm',
            'wp_ajax_send_form' => 'sendForm',
        );
    }

    /**
     * Send the form
     *
     * Gets registered to action in getActions()
     *
     * @return string  $response  Response of sending the form in JSON. Contains check boolean and response message.
     */
    public static function sendForm(): string
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->check = true;

            // Get form ID
            $this->form_id = $_POST['form_id'];

            // Build HTML
            $this->buildHtml();

            // Only continue if all fields are valid
            if ($this->check) {
                $this->sendMail();
            }
        } else {
            $this->response = 'No data received';
        }

        // Return the check and response in JSON
        echo json_encode(array('check' => $this->check, 'message' => $this->response));

        // Die for WP Ajax to work correctly
        wp_die();
    }

    /**
     * Build the HTML message
     *
     * Gets called in sendForm()
     */
    private function buildHtml()
    {
        // Build DomDocument
        $this->DOM = new \QB\QDOM();

        // Fill table with POST data
        $table = $this->createTable();

        // Build DOM message
        $html = $this->DOM->createElement('html');
            $body = $this->DOM->createElement('body');
            $body->appendChild($table);
        $html->appendChild($body);

        // Save DOM to message
        $this->DOM->appendChild($html);
    }

    /**
     * Send the form
     *
     * Gets called in buildHtml()
     *
     * @return object  $table  DOM Table filled with rows per form element
     */
    private function createTable(): \DOMElement
    {
        // Create table element
        $atts = array(
            'rules' => 'all',
            'style' => 'border-color: #666',
            'cellpadding' => '10',
            'width' => '600px',
        );
        $table = $this->DOM->generateElement('table', $atts);

        $i = 1;

        while (have_rows('inputs', $this->form_id)) :
            the_row();

            // Get input attributes
            $name  = get_sub_field('name');
            $type  = get_sub_field('type');
            $error = get_sub_field('error');
            $req   = get_sub_field('req');

            // Build ID and get input from POST
            $id = $type . '_' . $this->form_id . '_' . $i;
            $input = $_POST[$id];

            // Validate input if it exists
            if ($input) {
                $input = $this->validateField($input, $type, $error, $req);

                // Stop loop if field validation failed
                if (!$this->check) {
                    break;
                }
            }

            // Build HTML
            $atts = array('style' => 'background: #eee;');
            $tr = $this->DOM->generateElement('tr', $atts);

                $td = $this->DOM->createElement('td');
                $tr->appendChild($td);

                $strong = $this->DOM->generateElement('strong', null, $name);
                $td->appendChild($strong);

                $td = $this->DOM->generateElement('td', null, $input);
                $tr->appendChild($td);

            $table->appendChild($tr);

            $i++;
        endwhile;

        return $table;
    }

    /**
     * Validate a single field
     *
     * Gets called in createTable()
     *
     * @param string  $input  Input of user into field
     * @param string  $type   Type of the input
     * @param string  $error  Error message belonging to the field
     * @param bool    $req    If the field is required or not
     *
     * @return string  $input  Filtered and validated input of user
     */
    private function validateField(string $input, string $type, string $error, bool $req): string
    {
        // Strip input
        $input = $this->stripInput($input);

        // Validate required fields
        if ($req) {
            // Check if the field is not empty
            if (strlen($input) == 0 || ctype_space($input)) {
                // Set response to error message
                $this->response = $error;
                $this->check = false;
            }

            // Type specific checks
            switch ($type) {
                // Mail fields
                case 'mail':
                    if (!filter_var($input, FILTER_VALIDATE_EMAIL)) {
                        // Set response to error message
                        $this->response = $error;
                        $this->check = false;
                    }

                    break;

                default:

                    break;
            }
        }

        return $input;
    }
    
    /**
     * Strip input value
     *
     * Gets called in validateField()
     *
     * @param string  $input  Input of user
     *
     * @return string  $input  Stripped input of user
     */
    private function stripInput(string $input): string
    {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);

        return $input;
    }

    /**
     * Send the e-mail
     *
     * Gets called in sendForm()
     */
    private function sendMail()
    {
        // Get list of receivers
        $to = $this->getReceivers();

        // Text and subject
        $subject = get_field('subject', $this->form_id);
        $subject = ($subject == '') ? 'Reactie formulier' : $subject;

        // Add headers
        $domain = str_replace('https://', '', WP_HOME);
        $domain = str_replace('http://', '', $domain);
        $headers = array();
        $headers[] = 'From: Website ' . get_bloginfo('name') . ' <website@' . $domain . '>';
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        // Send mail
        $mail = wp_mail($to, $subject, $this->DOM->saveHTML(), $headers);
        $this->response = 'Bericht succesvol verzonden';
    }

    /**
     * Get list of receivers
     *
     * Gets called in sendMail()
     *
     * @return string  $receivers  String of e-mails seperated by comma
     */
    private function getReceivers(): string
    {
        $to = '';
        $receivers = get_field('receivers', $this->form_id);

        if (count($receivers) > 0) {
            // Loop over receivers if there are any
            foreach ($receivers as $receiver) {
                $to .= $receiver['receiver'] . ', ';
            }

            $to = substr($to, 0, -2);
        } else {
            // Else set to admin email
            $to = get_option('admin_email');
        }

        return $to;
    }
}
