<?php

namespace QB\RestAPI;

class Menu extends RestAPI
{
    /**
     * Construct with namespace
     */
    public function __construct()
    {
        $this->route = 'menu/(?P<slug>[a-zA-Z0-9_-]+)';
        $this->args = array();
    }

    /**
     * Get a menu.
     *
     * @param WP_Rest_Request $request
     *
     * @return array $restMenu
     */
    public function getCallback(\WP_REST_Request $request): array
    {
        // Save modified date for WP Route
        $routeOption = str_replace('/', '_', $request->get_route());
        $modDate = gmdate('D, d M Y H:i:s T', filectime(__FILE__));
        update_option($routeOption, $modDate);

        $slug = $request['slug'];
        $menuObject = $slug ? wp_get_nav_menu_object($slug) : array();
        $menuItems = $slug ? wp_get_nav_menu_items($slug) : array();

        $restMenu = array();

        if ($menuObject) {
            $menu = (array)$menuObject;
            $restMenu['ID'] = abs($menu['term_id']);
            $restMenu['name'] = $menu['name'];
            $restMenu['slug'] = $menu['slug'];

            $restMenuItems = array();
            foreach ($menuItems as $itemObject) {
                $restMenuItems[] = $this->getMenuItem($itemObject);
            }

            $restMenu['items'] = $restMenuItems;
        }

        return $restMenu;
    }

    /**
     * Format a menu item for REST API consumption.
     *
     * @param object $menuItem
     *
     * @return array $menuItem
     */
    private function getMenuItem(\WP_POST $menuItem): array
    {
        $item = (array)$menuItem;

        $menuItem = array(
            'id' => (int)$item['ID'],
            'title' => $item['title'],
            'url' => str_replace(WP_HOME, '', $item['url']),
            'target' => $item['target'],
        );

        return $menuItem;
    }
}
