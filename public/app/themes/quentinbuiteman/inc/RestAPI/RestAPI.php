<?php

namespace QB\RestAPI;

abstract class RestAPI implements \QB\PluginAPI\ActionHook
{
    /**
     * @var string $namespace Namespace to use in all routes
     * @var string $route     The route
     * @var string $args      Arguments for the route
     */
    protected $namespace = 'qb/v2';
    protected $route;
    protected $args;

    /**
     * Add filters
     *
     * @return array
     */
    public static function getActions(): array
    {
        return array( 'rest_api_init' => 'register' );
    }


    /**
     * Add filters
     */
    public function register()
    {
        register_rest_route($this->namespace, $this->route, array(
            'methods' => \WP_REST_Server::READABLE,
            'callback' => array($this, 'getCallback'),
            'args' => $this->args,
        ));
    }

    /**
     * Get callback
     *
     * @return array
     */
    abstract public function getCallback(\WP_REST_Request $request): array;
}
