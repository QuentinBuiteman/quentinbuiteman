<?php

namespace QB\RestAPI;

class Page extends RestAPI
{
    /**
     * @var int $ID ID of the page to get data from
     */
    private $ID;
    private $page;

    /**
     * Construct with namespace
     */
    public function __construct()
    {
        $this->route = 'page/(?P<slug>[a-zA-Z0-9_-]+)';
        $this->args = array();
    }

    /**
     * Build callback from Request
     *
     * @param WP_Rest_Request $request The request made to the WP Rest API
     *
     * @return array $content Content data of the page
     */
    public function getCallback(\WP_REST_Request $request): array
    {
        // Set ID based on slug
        $slug = str_replace('--', '/', $request['slug']);
        $post_data = get_page_by_path($slug);
        $this->ID = $post_data->ID;

        // Setup global post for shortcodes
        global $post;
        $post = $post_data;
        setup_postdata($post);

        // Save modified date for WP Route
        $routeOption = str_replace('/', '_', $request->get_route());
        $modTime = get_the_modified_time('U');
        $modDate = gmdate('D, d M Y H:i:s T', $modTime);
        update_option($routeOption, $modDate);

        // Get all the fields
        $ACF = new ACF($this->ID);
        $content = $ACF->getFields();
        $content['title'] = htmlspecialchars_decode($this->getTitle());

        return $content;
    }

    /**
     * Get title of page
     *
     * Gets called in getFields()
     *
     * @return string $title Title of the page
     */
    private function getTitle(): string
    {
        // Get yoast title
        $title = get_post_meta($this->ID, '_yoast_wpseo_title', true);

        // Build own title if it doesn't exist
        if ($title == '') {
            $title = get_the_title($this->ID) . ' - ' . get_bloginfo('name');
        }

        return $title;
    }
}
