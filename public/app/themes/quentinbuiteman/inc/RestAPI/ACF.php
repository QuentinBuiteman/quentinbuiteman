<?php

namespace QB\RestAPI;

class ACF
{
    /**
     * @var int $ID ID of the page to get data from
     */
    private $ID;
    public $fields;

    /**
     * Construct object
     *
     * @param mixed $ID ID of the page to get data for
     */
    public function __construct($ID = 0)
    {
        $this->ID = $ID;
    }

    /**
     * Get all fields
     *
     * @return array $fields Fields data of the page
     */
    public function getFields(): array
    {
        // Get all ACF fields
        $this->fields = get_fields($this->ID);

        if ($this->fields) {
            $this->fields = $this->filterValues($this->fields);
        }

        return $this->fields;
    }

    /**
     * Filter field values for correct data
     *
     * Gets called in filterFields()
     *
     * @param array $fields Fields without filter applied
     *
     * @return array $fields Fields with filter applied
     */
    private function filterValues(array $fields): array
    {
        foreach ($fields as $key => $value) {
            switch ($value) {
                case is_int($value):
                    $fields[$key] = $this->imgOutput($value);

                    break;

                case is_array($value):
                    $fields[$key] = $this->filterValues($value);

                    break;
            }
        }

        // Return the filtered fields
        return $fields;
    }

    /**
     * Filter value to inline SVG or add background image
     *
     * Gets called in filterValues()
     *
     * @param int $value Value without filter applied
     *
     * @return mixed $value Embedded image or original value if int isn reffering to an image
     */
    private function imgOutput($value)
    {
        // Get url
        $url = get_attached_file($value);

        // Only run filter if it is an image
        if ($url) {
            // Get file extension
            $exp = explode('.', $url);
            $ext = end($exp);

            /**
             * If SVG: get code
             */
            if ($ext === 'svg') {
                $value = file_get_contents($url);
            }
        }

        return $value;
    }
}
