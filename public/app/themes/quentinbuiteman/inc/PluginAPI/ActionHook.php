<?php

namespace QB\PluginAPI;

/**
 * Subscribe to an WP action hook
 */
interface ActionHook
{
    /**
     * Subscribes object functions to actions
     *
     * Example returns:
     *     array('action_name' => 'method')
     *     array('action_name' => array('method', $prio))
     *     array('action_name' => array('method', $prio, $args))
     *
     * @return array
     */
    public static function getActions(): array;
}
