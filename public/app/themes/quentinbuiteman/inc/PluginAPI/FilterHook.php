<?php

namespace QB\PluginAPI;

/**
 * Subscribe to a WP filter hook
 */
interface FilterHook
{
    /**
     * Subscribe object functions to filters
     *
     * Example returns:
     *     array('filter_name' => 'method')
     *     array('filter_name' => array('method', $prio))
     *     array('filter_name' => array('method', $prio, $args))
     *
     * @return array
     */
    public static function getFilters(): array;
}
