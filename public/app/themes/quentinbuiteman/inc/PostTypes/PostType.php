<?php

namespace QB\PostTypes;

/**
 * Abstract class used for all post types registrations
 */
abstract class PostType implements \QB\PluginAPI\ActionHook
{
    /**
     * PostType variables
     *
     * @var string  $name    Name of the post type to register
     * @var array   $labels  Labels belonging to the post type
     * @var array   $args    Arguments belonging to the post type
     */
    protected $name;
    protected $labels;
    protected $args;

    /**
     * Run functions to set labels and args
     *
     * @param string  $name  Name of the post type to register
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->setLabels();
        $this->setArgs();
    }

    /**
     * Subscribe functions to corresponding actions
     *
     * @return array  Consists of 'action_to_subscribe_to' => 'function_to_run'
     */
    public static function getActions(): array
    {
        return array('admin_init' => 'register');
    }

    /**
     * Register post type
     *
     * @return mixed  WP_Post_Type on succes, WP_Error on failure
     */
    public function register()
    {
        register_post_type($this->name, $this->args);
    }

    /**
     * Set labels
     */
    abstract protected function setLabels();

    /**
     * Set arguments
     */
    abstract protected function setArgs();
}
