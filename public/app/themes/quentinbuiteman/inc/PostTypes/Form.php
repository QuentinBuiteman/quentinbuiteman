<?php

namespace QB\PostTypes;

/**
 * Create labels and arguments for a Form post type registration
 */
class Form extends PostType
{
    /**
     * Set labels
     */
    protected function setLabels()
    {
        $this->labels = array(
            'name' => 'Form',
            'singular_name' => 'Form',
            'add_new' => 'New',
            'add_new_item' => 'New form',
            'edit_item' => 'Edit form',
            'new_item' => 'Nieuw form',
            'view_item' => 'Bekijk form',
            'search_items' => 'Zoek form',
            'not_found' => 'Niets gevonden',
            'not_found_in_trash' => 'Niets gevonden in de prullenbak',
            'menu_name' => 'Forms',
        );
    }

    /**
     * Set arguments
     */
    protected function setArgs()
    {
        $this->args = array(
            'labels' => $this->labels,
            'hierarchical' => true,
            'description' => 'Forms',
            'supports' => array('title', 'thumbnail'),
            'show_ui' => true,
            'menu_position' => 21,
            'menu_icon' => 'dashicons-forms',
            'show_in_nav_menus' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => true,
            'capability_type' => 'page',
        );
    }
}
