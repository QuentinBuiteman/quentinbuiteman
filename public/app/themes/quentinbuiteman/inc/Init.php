<?php

namespace QB;

class Init
{
    /**
     * @var CleanMCE    $clean_mce       Clean the MCE editor
     * @var Setup       $setup           Initial theme setup
     *
     * @var SendForm    $send_form       Ajax call to send a form
     *
     * @var Enqueue     $enq_files       Enqueue files
     * @var Head        $head            Custom code to the wp_head
     *
     * @var Menu        $menu            WP-Rest to get base menu data
     * @var Page        $page            WP-Rest to get ACF data from page
     *
     * @var Form        $form_post_type  Initialize the form post type
     */
    private $clean_mce;
    private $setup;

    private $send_form;

    private $enq_files;
    private $head;

    private $menu;
    private $page;

    private $form_post_type;

    /**
     * Constructor
     *
     * Initializes all classes
     * Runs the register function
     * Registers all shortcodes
     */
    public function __construct()
    {
        // Admin
        $this->clean_mce = new Admin\CleanMCE();
        $this->setup = new Admin\Setup();

        // Ajax calls
        $this->send_form = new Ajax\SendForm();

        // Render
        $this->enq_files = new Render\Enqueue();
        $this->head = new Render\Head();

        // Rest API
        $this->menu = new RestAPI\Menu();
        $this->page = new RestAPI\Page();

        // Post types
        $this->form_post_type = new PostTypes\Form('form');
        $this->form_post_type->register();

        // Run PluginAPI registration
        $this->registerAll();

        // Register shortcodes
        Shortcodes\Form::register('form');
    }

    /**
     * Loop over properties to register them via PluginAPI
     */
    private function registerAll()
    {
        $api_manager = new PluginAPI\Manager();

        foreach ($this as $property => $class) {
            $api_manager->register($class);
        }
    }
}
