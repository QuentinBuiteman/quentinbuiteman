<?php

namespace QB\Render;

class Head implements \QB\PluginAPI\ActionHook
{
    /**
     * Subscribe functions to corresponding actions
     *
     * @return array
     */
    public static function getActions(): array
    {
        return array (
            'wp_head' => 'generate',
        );
    }

    /**
     * Add GA code if it's there
     */
    private function addAnalytics()
    {
        if (WP_ENV == 'production') {
            if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) {
                if (get_field('add_ga', 'options')) {
                    the_field('ga_code', 'options');
                }
            }
        }
    }

    /**
     * Add some features to the head of the document
     */
    public function generate()
    {
        // Set meta for responsive
        echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";

        // Add favicons
        include_once( DOC_ASSETS . '/favicons/favicons.html' );

        // Disable Google Pagespeed Insights
        $this->addAnalytics();
    }
}
