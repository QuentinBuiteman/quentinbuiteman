<?php

namespace QB\Render;

class Enqueue implements \QB\PluginAPI\ActionHook, \QB\PluginAPI\FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     *
     * @return array
     */
    public static function getActions(): array
    {
        return array (
            'wp_enqueue_scripts' => 'enqueue',
        );
    }

    /**
     * Subscribe functions to corresponding filters
     *
     * @return array
     */
    public static function getFilters(): array
    {
        return array (
            'clean_url' => array('defer', 11),
        );
    }
 
    /**
     * Add defer tags to JS scripts
     *
     * @param string $url
     *
     * @return string $url
     */
    public function defer(string $url): string
    {
        if (strpos($url, '.js')) {
            $url = !is_admin() ? "$url' defer='defer" : $url;
        }

        return $url;
    }

    /**
     * Enqueue scripts and styles.
     */
    public function enqueue()
    {
        // Deregister Embed script
        wp_deregister_script('wp-embed');

        // Load our main script
        wp_enqueue_script('vendor', WP_ASSETS . '/js/vendor.js', array(), '1.5.1', true);

        // Load our main script
        wp_enqueue_script('bundle', WP_ASSETS . '/js/bundle.js', array(), '1.5.2', true);
    }
}
