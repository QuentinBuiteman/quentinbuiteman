<?php

namespace QB\Admin;

/**
 * Give the TinyMCE editor custom features
 *
 * Adds own styles
 * Adjusts buttons on toolbars
 */
class CleanMCE implements \QB\PluginAPI\FilterHook
{
    /**
     * Subscribe functions to corresponding filters
     *
     * @return array Consists of 'filter_to_subscribe_to' => 'function_to_run'
     */
    public static function getFilters(): array
    {
        return array(
            'acf/fields/wysiwyg/toolbars' => 'customToolbars',
            'tiny_mce_before_init' => 'customStyles',
        );
    }


    /**
     * Add custom styles to TinyMCE editor
     *
     * Gets registered in getFilters()
     *
     * @param array $settings Standard settings for the TinyMCE editor
     *
     * @return array $settings Custom settings for the TinyMCE editor
     */
    public function customStyles(array $settings): array
    {
        // Always show bottom toolbar with styles
        $settings['wordpress_adv_hidden'] = false;

        // Add styles
        $styleFormats = array(
            array(
                'title' => 'Font XL',
                'selector' => 'h1, h2, h3, h4, p, ul, ol, small, a',
                'classes' => 'fs-xl',
            ),
            array(
                'title' => 'Font L',
                'selector' => 'h1, h2, h3, h4, p, ul, ol, small, a',
                'classes' => 'fs-l',
            ),
            array(
                'title' => 'Font M',
                'selector' => 'h1, h2, h3, h4, p, ul, ol, small, a',
                'classes' => 'fs-m',
            ),
            array(
                'title' => 'Font S',
                'selector' => 'h1, h2, h3, h4, p, ul, ol, small, a',
                'classes' => 'fs-s',
            ),
            array(
                'inline' => 'span',
                'title' => 'White text',
                'classes' => 'clr--white',
            ),
        );

        $settings['style_formats'] = json_encode($styleFormats);

        return $settings;
    }


    /**
     * Make custom toolbars
     *
     * Gets registered in getFilters()
     *
     * @param array $toolbars Standard toolbars for the TinyMCE editor
     *
     * @return array $toolbars Custom toolbars for the TinyMCE editor
     */
    public function customToolbars(array $toolbars): array
    {
        $tbTop = $toolbars['Full'][1];
        $tbBot = $toolbars['Full'][2];

        // Hide unnecessary buttons from top toolbar
        $hideBtns = array('wp_adv');
        $tbTop = $this->hideFromToolbar($tbTop, $hideBtns);

        // Hide unnecessary buttons from bottom toolbar
        $hideBtns = array(
            'fontselect',
            'fontsizeselect',
            'outdent',
            'indent',
            'wp_more',
            'forecolor',
            'table',
        );
        $tbBot = $this->hideFromToolbar($tbBot, $hideBtns);

        // Add styleselect button to bottom toolbar
        $showBtns = array('styleselect' => 0);
        $tbBot = $this->showOnToolbar($tbBot, $showBtns);

        // Add result to original array
        $toolbars['Full'][1] = $tbTop;
        $toolbars['Full'][2] = $tbBot;

        // return $toolbars
        return $toolbars;
    }


    /**
     * Remove buttons from list of buttons
     *
     * Gets called in customToolbars()
     *
     * @param array  $tbBtns    Current buttons on the toolbar
     * @param array  $hideBtns  Buttons to hide on the toolbar
     *
     * @return array  $tbBtns    Updated buttons for the toolbar
     */
    private function hideFromToolbar(array $tbBtns, array $hideBtns): array
    {
        // Hide toggle button
        foreach ($tbBtns as $key => $value) {
            if (in_array($value, $hideBtns)) {
                unset($tbBtns[$key]);
            }
        }

        return $tbBtns;
    }


    /**
     * Remove buttons from list of buttons
     *
     * Gets called in customToolbars()
     *
     * @param array  $tbBtns    Current buttons on the toolbar
     * @param array  $showBtns  Buttons to show on the toolbar
     *
     * @return array  $tbBtns    Updated buttons for the toolbar
     */
    private function showOnToolbar(array $tbBtns, array $showBtns): array
    {
        // Hide toggle button
        foreach ($showBtns as $name => $loc) {
            array_splice($tbBtns, $loc, 0, $name);
        }

        return $tbBtns;
    }
}
