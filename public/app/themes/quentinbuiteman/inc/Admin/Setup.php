<?php

namespace QB\Admin;

/**
 * Standard functionality for the theme
 *
 * Disable core updates
 * Add mime types
 * Remove uneccesary data from wp_head
 * Add favicon to back-end
 * Register menu
 * Add options page
 * Hide specific WordPress items via CSS
 */
class Setup implements \QB\PluginAPI\ActionHook, \QB\PluginAPI\FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     *
     * @return array  Consists of 'action_to_subscribe_to' => 'function_to_run'
     */
    public static function getActions(): array
    {
        return array (
            'after_setup_theme' => 'defaults',
            'editable_roles' => 'userRoleDropdown',
            'admin_head' => 'hideElements',
            'save_post' => 'purgeVarnish',
        );
    }

    /**
     * Subscribe functions to corresponding filters
     *
     * @return array  Consists of 'filter_to_subscribe_to' => 'function_to_run'
     */
    public static function getFilters(): array
    {
        return array (
            'wp_check_filetype_and_ext' => array('checkFiletype', 100, 4),
            'wpseo_metabox_prio' => 'yoastToBottom',
            'upload_mimes' => 'addSvgMime',
            'rest_pre_dispatch' => array('restCaching', 10, 3),
        );
    }

    /**
     * SVG for Media library
     *
     * @param array  $mimes  List of mime types
     *
     * @return array  $mimes  Updated list of mime types
     */
    public function addSvgMime(array $mimes): array
    {
        $mimes['svg'] = 'application/svg+xml';

        return $mimes;
    }

    /**
     * Set SVG to image/svg+xml after first check
     *
     * @param array   $data       Data belonging to the file
     * @param string  $file_tmp   Temp file name
     * @param string  $file_name  Name of the file
     * @param mixed   $mimes      List of mime types
     *
     * @return array  $data  Updated data belonging to the file
     */
    public function checkFiletype(array $data, string $file_tmp, string $file_name, $mimes): array
    {
        if (substr($file_name, -4) === '.svg') {
            $data['ext'] = 'svg';
            $data['type'] = 'image/svg+xml';
        }

        return $data;
    }

    /**
     * Set caching headers for REST requests
     *
     * @param mixed            $result   Result of REST request
     * @param WP_REST_Server   $server   The REST Server
     * @param WP_REST_Request  $request  The REST request
     *
     * @return mixed  $result  Original result to continue with REST request
     */
    public function restCaching($result, \WP_REST_Server $server, \WP_REST_Request $request)
    {
        $route = $request->get_route();
        $routeOption = str_replace('/', '_', $route);

        $expire = gmdate('D, d M Y H:i:s T', time() + 7200);

        $server->send_headers(array(
            'Cache-Control' => 'public, max-age=7200',
            'Expires' => $expire,
            'Last-Modified' => get_option($routeOption),
        ));

        return $result;
    }

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @return mixed  Success or failure of all functions
     */
    public function defaults()
    {
        // Add theme options to editor role
        $editor = get_role('editor');
        $editor->add_cap('edit_theme_options');
        $editor->add_cap('create_users');
        $editor->add_cap('edit_users');
        $editor->add_cap('delete_users');
        $editor->add_cap('list_users');

        // Hide admin bar
        show_admin_bar(false);

        // Add theme supports
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('html5', array('search-form'));

        // Updates
        add_filter('auto_update_core', '__return_true');
        add_filter('auto_update_plugin', '__return_true');

        // Remove unecessary files from the page
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'wp_shortlink_wp_head');
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary'   => 'Primair menu',
        ));

        // This theme styles the visual editor to resemble the theme style.
        add_editor_style(array('css/editor-style.css'));

        // Option pages for extra text
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(
                array(
                    'page_title' => 'Admin',
                    'menu_title' => 'Admin',
                    'menu_slug'  => 'admin',
                    'capability' => 'create_users',
                )
            );
        }
    }

    /**
     * Don't allow editors to make users admin
     *
     * @param array  $all_roles  All roles for dropdown
     *
     * @return array  $all_roles  All roles for dropdown without admin
     */
    public function userRoleDropdown($all_roles): array
    {
        global $pagenow;

        // if current user is editor AND current page is edit/new user page
        if (current_user_can('editor') && ($pagenow == 'user-edit.php' || $pagenow == 'user-new.php')) {
            unset($all_roles['administrator']);
        }

        return $all_roles;
    }

    /**
     * Hide elements in WP Admin via CSS
     *
     * @return void
     */
    public function hideElements()
    {
        if (WP_ENV === 'production') {
            if (!current_user_can('update_core')) {
                remove_action('admin_notices', 'update_nag', 3);
            }
        }

        $style = '<style>' .
                     '#postdivrich,' .
                     '.mce-menubar,' .
                     '.media-sidebar .setting[data-setting="caption"],' .
                     '.media-sidebar .setting[data-setting="description"],' .
                     '.media-types.media-types-required-info,' .
                     '.attachment-display-settings {' .
                         'display: none !important;' .
                     '}' .
                 '</style>';

        echo $style;

        echo '<link href="' . WP_ASSETS . '/favicons/favicon.ico" rel="shortcut icon">';
    }

    /**
     * Move yoast to bottom of page
     */
    public function yoastToBottom(): string
    {
        return 'low';
    }

    /**
     * Purge varnish cache when post is saved
     *
     * @param int  $ID  ID of post to purge
     */
    public function purgeVarnish(int $ID)
    {
        wp_remote_request(esc_url(get_permalink($ID)), array('method' => 'PURGE'));
    }
}
