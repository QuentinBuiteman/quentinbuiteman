<?php

namespace QB\Shortcodes;

/**
 * Used for registration of all shortcodes
 */
abstract class Shortcode
{
    /**
     * @var array   $atts     Attributes added in shortcode
     * @var string  $content  Content inside shortcode
     */
    protected $atts;
    protected $content;

    /**
     * Add the shortcode to WP and start build
     *
     * @param string  $name  Name of the shortcode to register
     */
    public static function register(string $name)
    {
        $self = new static();
        add_shortcode($name, array($self, 'handle'));
    }

    /**
     * Add attributes and content to instance and start generation
     *
     * @param array   $atts     Attributes added in shortcode
     * @param string  $content  Content inside shortcode
     *
     * @return string  Generated HTML for shortcode
     */
    public function handle($atts = null, $content = null): string
    {
        $this->atts = $atts;
        $this->content = $content;

        return $this->generate();
    }

    /**
     * Start generation process
     *
     * @return string  Generated HTML for shortcode
     */
    abstract protected function generate(): string;
}
