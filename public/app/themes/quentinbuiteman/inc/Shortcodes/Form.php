<?php

namespace QB\Shortcodes;

class Form extends Shortcode
{
    /**
     * @var int  $ID    ID of the form
     * @var QDOM $DOM   DOMDocument for HTML generation
     * @var int  $count Keep count of loop over fields
     */
    private $ID;
    private $DOM;
    private $count;

    /**
     * Get form info and start build process
     *
     * @return string HTML of form to generate
     */
    protected function generate(): string
    {
        $name = $this->atts['name'];

        $form = get_page_by_title($name, object, 'form');
        $this->ID = $form->ID;

        return $this->form();
    }

    /**
     * Build form
     *
     * @return <html> string
     */
    private function form(): string
    {
        // Build DomDocument
        $this->DOM = new \QB\QDOM();

        // Create form tag
        $atts = array(
            'class' => 'form',
            'method' => 'post',
            'action' => admin_url('admin-ajax.php'),
            'data-id' => $this->ID,
            'data-action' => 'send_form',
            'enctype' => 'multipart/form-data',
            'novalidate' => 'novalidate'
        );
        $form = $this->DOM->generateElement('form', $atts);
        $this->DOM->appendChild($form);

        // Create main section
        $atts = array('class' => 'form__fields');
        $section = $this->DOM->generateElement('section', $atts);
        $form->appendChild($section);

        // Create fields
        $fields = $this->fields();
        foreach ($fields as $field) {
            $section->appendChild($field);
        }

        // Create footer section
        $footer = $this->footer();
        $form->appendChild($footer);

        // Save HTML and return
        return $this->DOM->saveHTML();
    }

    /**
     * Get fields and build
     *
     * Gets called in form()
     *
     * @return array $field Array of DOM elements with field data
     */
    private function fields(): array
    {
        $this->count = 1;
        $fields = array();

        // Loop over all fields
        while (have_rows('inputs', $this->ID)) :
            the_row();

            // Variables
            $name = get_sub_field('name');
            $type = get_sub_field('type');
            $error = get_sub_field('error');
            $req = get_sub_field('req');

            // Build ID
            $ID = $type . '_' . $this->ID . '_' . $this->count;

            // Build field
            $atts = array('class' => 'field ' . $type);
            $field = $this->DOM->generateElement('div', $atts);

            // Get inputs based on type
            $elem = $this->elem($ID, $name, $type, $req);
            $field->appendChild($elem);

            // Build error message
            if ($req) {
                $atts = array('class' => 'field__error');
                $span = $this->DOM->generateElement('span', $atts, $error);
                $field->appendChild($span);
            }

            // Build label
            $atts  = array(
                'for' => $ID,
                'class' => 'field__label',
            );
            $label = $this->DOM->generateElement('label', $atts, $name);
            $field->appendChild($label);

            // Add field to array of fields
            $fields[] = $field;

            $this->count++;
        endwhile;

        return $fields;
    }

    /**
     * Get elements based on type
     *
     * Gets called in fields()
     *
     * @param string $ID   Unique ID of the field
     * @param string $name Label/placeholder
     * @param string $type Type of the field
     * @param bool   $req  If the field is required
     *
     * @return DOMElement $elem DOM element based on type
     */
    private function elem(string $ID, string $name, string $type, bool $req): \DOMElement
    {
        switch ($type) {
            // Textarea
            case 'textarea':
                $elem = $this->textarea($ID, $name, $req);

                break;

            // Default input
            default:
                $elem = $this->input($ID, $name, $type, $req);

                break;
        }

        return $elem;
    }

    /**
     * Build textarea
     *
     * Gets called in elem()
     *
     * @param string $ID   Unique ID of the field
     * @param string $name Label/placeholder
     * @param bool   $req  If the field is required
     *
     * @return DOMElement $textarea Textarea DOM element
     */
    private function textarea(string $ID, string $name, bool $req): \DOMElement
    {
        // Textarea attributes
        $atts = array(
            'id' => $ID,
            'name' => $ID,
            'class' => 'field__input',
            'placeholder' => $name,
            'tabindex' => $this->ID . $this->count,
            'rows' => 5,
        );

        // Only require if necessary
        if ($req) {
            $atts['required'] = 'required';
        }

        // Create textarea with attributes
        $textarea = $this->DOM->generateElement('textarea', $atts);

        return $textarea;
    }

    /**
     * Build standard input
     *
     * Gets called in elem()
     *
     * @param string $ID   Unique ID of the field
     * @param string $name Label/placeholder
     * @param string $type Type of the field
     * @param bool   $req  If the field is required
     *
     * @return DOMElement $input DOM Input element
     */
    private function input(string $ID, string $name, string $type, bool $req): \DOMElement
    {
        // Set input attributes
        $atts = array(
            'id' => $ID,
            'name' => $ID,
            'class' => 'field__input',
            'placeholder' => $name,
            'tabindex' => $this->ID . $this->count,
            'type' => $type,
        );

        // Only require if necessary
        if ($req) {
            $atts['required'] = 'required';
        }

        // Create input with attributes
        $input = $this->DOM->generateElement('input', $atts);

        return $input;
    }

    /**
     * Build form footer
     *
     * Gets called in form()
     *
     * @return DOMElement $section DOM element with form footer (submit, loader, checkmark)
     */
    private function footer(): \DOMElement
    {
        // Create section
        $atts = array('class' => 'form__foot');
        $section = $this->DOM->generateElement('section', $atts);

        // Submit div for alignment
        $atts = array('class' => 'form__submit');
        $div = $this->DOM->generateElement('div', $atts);
        $section->appendChild($div);

        // Submit button
        $submit = get_field('submit_btn', $this->ID);
        $atts = array(
            'class' => 'btn',
            'value' => $submit,
            'tabindex' => $this->ID . $this->count,
            'type' => 'submit'
        );
        $input = $this->DOM->generateElement('input', $atts);
        $div->appendChild($input);
        
        // Checkmark
        $atts = array('class' => 'form__check');
        $check = $this->DOM->generateElement('div', $atts);
        $div->appendChild($check);
        
        // Checkmark
        $atts = array('class' => 'form__loader');
        $check = $this->DOM->generateElement('div', $atts);
        $div->appendChild($check);

        return $section;
    }
}
