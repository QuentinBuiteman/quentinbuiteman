// Import libraries
const fs = require('fs');
const favicons = require('favicons');
const del = require('del');

// Source for generation
const assets = 'app/themes/quentinbuiteman/assets/favicons';
const wassets = `public/${assets}`;
const source = `${wassets}/favicon.svg`;

// Configuration
const config = {
  appName: 'Quentin Buiteman',
  appDescription: 'The personal website of Quentin Buiteman',
  developerName: 'Quentin Buiteman',
  developerURL: 'https://quentinbuiteman.com',
  background: '#ffffff',
  theme_color: '#e34d42',
  path: `/${assets}/`,
  display: 'browser',
  orientation: 'portrait',
  start_url: '/',
  version: 1.0,
  logging: false,
  online: false,
  html: 'favicons.html',
  pipeHTML: true,
  replace: true,
  icons: {
    android: true,
    appleIcon: { offset: 6 },
    appleStartup: true,
    coast: { offset: 6 },
    favicons: true,
    firefox: { offset: 6 },
    windows: true,
    yandex: true,
  },
};

// Callback after generation
const callback = (error, response) => {
  // Print error
  // Else write response to files
  if (error) {
    console.log(error);
  } else {
    // Images
    response.images.map((image) => {
      fs.writeFile(`${wassets}/${image.name}`, image.contents, (err) => {
        if (err) return console.log(err);
      });
    });

    // Files
    response.files.map((file) => {
      fs.writeFile(`${wassets}/${file.name}`, file.contents, (err) => {
        if (err) return console.log(err);
      });
    });
  }
};

// Delete everything in favicons map besides favicon.svg
del([`${wassets}/**`, `!${wassets}`, `!${source}`]);

// Run favicon generation
favicons(source, config, callback);
