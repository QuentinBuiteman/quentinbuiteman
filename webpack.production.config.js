// Import
const webpack = require('webpack');
const path = require('path');

// Paths variables
const themePath = 'public/app/themes/quentinbuiteman';
const buildDir = path.resolve(__dirname, `${themePath}/assets`);
const appDir = path.resolve(__dirname, `${themePath}/src`);

module.exports = {
  // Entry of our app
  entry: { bundle: `${appDir}/index.js` },

  // Output path and filename
  output: {
    path: buildDir,
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].js',
  },

  // Add modules
  module: {
    rules: [
      {
        test: /\.js$/,
        include: appDir,
        loader: 'babel-loader',
      },
      {
        test: /\.svg$/,
        use: [
          'babel-loader',
          'svg-react-loader',
        ],
      },
    ],
  },

  // Standard part for img imports
  resolve: {
    modules: [
      path.resolve(__dirname),
      'node_modules',
    ],
    alias: {
      img: `${buildDir}/img`,
      src: appDir,
      general: `${appDir}/general`,
      pages: `${appDir}/pages`,
      components: `${appDir}/components`,
      containers: `${appDir}/containers`,
    },
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: ({ resource }) => /node_modules/.test(resource),
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        properties: true,
        sequences: true,
        dead_code: true,
        conditionals: true,
        comparisons: true,
        evaluate: true,
        booleans: true,
        unused: true,
        loops: true,
        hoist_funs: true,
        cascade: true,
        if_return: true,
        join_vars: true,
        drop_debugger: true,
        unsafe: true,
        hoist_vars: true,
        negate_iife: true,
      },
      mangle: {
        toplevel: true,
        sort: true,
        eval: true,
        properties: true,
      },
      output: {
        space_colon: false,
        comments: (node, comment) => {
          const text = comment.value;
          const type = comment.type;
          if (type === 'comment2') {
            return /@copyright/i.test(text);
          }
        },
      },
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
  ],
};
