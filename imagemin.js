// Import libraries
const imagemin = require('imagemin');
const imageminGif = require('imagemin-gifsicle');
const imageminJpg = require('imagemin-mozjpeg');
const imageminPng = require('imagemin-pngquant');
const imageminSvg = require('imagemin-svgo');

// Set paths
const folders = [
  'public/app/uploads',
  'public/app/themes/react/assets/img',
  'public/app/themes/react/assets/favicons',
];

// Imagemin everything
folders.map((path) => {
  imagemin([`${path}/*.{jpg,png,svg,gif}`], path, {
    plugins: [
      imageminGif({
        interlaced: true,
        optimizationLevel: 3,
      }),
      imageminJpg(),
      imageminPng(),
      imageminSvg(),
    ],
  }).then(() => {
    console.log(`Images optimized in ${path}`);
  });
});
