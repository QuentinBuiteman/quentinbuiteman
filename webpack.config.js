// Import
const webpack = require('webpack');
const path = require('path');
const StyleLintPlugin = require('stylelint-webpack-plugin');

// Paths variables
const themePath = 'public/app/themes/quentinbuiteman';
const buildDir = path.resolve(__dirname, `${themePath}/assets`);
const appDir = path.resolve(__dirname, `${themePath}/src`);

module.exports = {
  // Entry of our app
  entry: { bundle: `${appDir}/index.js` },

  // Output path and filename
  output: {
    path: buildDir,
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].js',
  },

  // Add modules
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        loader: 'eslint-loader',
      },
      {
        test: /\.js$/,
        include: appDir,
        loader: 'babel-loader',
      },
      {
        test: /\.svg$/,
        use: [
          'babel-loader',
          'svg-react-loader',
        ],
      },
    ],
  },

  // Standard part for img imports
  resolve: {
    modules: [
      path.resolve(__dirname),
      'node_modules',
    ],
    alias: {
      img: `${buildDir}/img`,
      src: appDir,
      general: `${appDir}/general`,
      pages: `${appDir}/pages`,
      components: `${appDir}/components`,
      containers: `${appDir}/containers`,
    },
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: ({ resource }) => /node_modules/.test(resource),
    }),
    new StyleLintPlugin({
      configFile: '.stylelintrc',
      context: appDir,
      files: ['**/*.js'],
      syntax: 'scss',
    }),
  ],
};
