<?php

$root_dir = dirname(__DIR__);
define('DOC_ROOT', $root_dir . '/public');
define('DOC_ASSETS', DOC_ROOT . '/app/themes/quentinbuiteman/assets');

/** Setup env **/
Env::init();

/** Setup phpdotenv **/
$dotenv = new Dotenv\Dotenv($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_PREFIX', 'WP_HOME']);
}

/** Production or development variables **/
define('WP_ENV', env('WP_ENV') ?: 'production');
$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';
if (file_exists($env_config)) {
    require_once $env_config;
}

/** Database links **/
define('WP_HOME', env('WP_HOME'));
define('WP_SITEURL', WP_HOME . '/wp');
define('WP_ASSETS', WP_HOME . '/app/themes/quentinbuiteman/assets');

/** DB settings **/
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST') ?: 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
define('WP_DEBUG', env('WP_DEBUG'));
$table_prefix = env('DB_PREFIX');

/** Custom locations **/
define('WP_CONTENT_DIR', DOC_ROOT . '/app');
define('WP_CONTENT_URL', WP_HOME . '/app');

/** Theme settings */
define('WP_DEFAULT_THEME', 'quentinbuiteman');
define('DISALLOW_FILE_EDIT', true);
define('WP_POST_REVISIONS', 1);

/** Salt **/
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));

/** Varnish HTTPS Forward **/
if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

/** Bootstrap Wordpress **/
if (!defined('ABSPATH')) {
    define('ABSPATH', DOC_ROOT . '/wp/');
}
